CREATE TABLE booking (
    id_request_booking UUID NOT NULL PRIMARY KEY,
    id_platform VARCHAR(100) NOT NULL,
    nama_platform VARCHAR(100) NOT NULL,
    doc_type VARCHAR(100) NOT NULL,
    term_of_payment VARCHAR(100) NOT NULL
);