package com.daffapradana.daffapradana.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

// Booking model
public class Booking {

    // Attributes
    private final UUID idRequestBooking;

    @NotBlank
    private final String idPlatform;
    @NotBlank
    private final String namaPlatform;
    @NotBlank
    private final String docType;
    @NotBlank
    private final String termOfPayment;

    // Setter (Constructor)
    public Booking(@JsonProperty("id_request_booking") UUID idRequestBooking,
                   @JsonProperty("id_platform") String idPlatform,
                   @JsonProperty("nama_platform") String namaPlatform,
                   @JsonProperty("doc_type") String docType,
                   @JsonProperty("term_of_payment") String termOfPayment) {
        this.idRequestBooking = idRequestBooking;
        this.idPlatform = idPlatform;
        this.namaPlatform = namaPlatform;
        this.docType = docType;
        this.termOfPayment = termOfPayment;
    }

    // Getter
    public UUID getIdRequestBooking() { return idRequestBooking; }
    public String getIdPlatform() { return idPlatform; }
    public String getNamaPlatform() { return namaPlatform; }
    public String getDocType() { return docType; }
    public String getTermOfPayment() { return termOfPayment; }
}
