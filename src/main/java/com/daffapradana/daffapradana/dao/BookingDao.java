package com.daffapradana.daffapradana.dao;

import com.daffapradana.daffapradana.model.Booking;

import java.util.UUID;

public interface BookingDao {

    // Define Create Booking
    int createBooking(UUID idRequestBooking, Booking booking);

    // Create Booking (Randomize UUID)
    default int createBooking(Booking booking) {
        // Randomize Booking ID
        UUID idRequestBooking = UUID.randomUUID();

        // Returns createBooking()
        return createBooking(idRequestBooking, booking);
    }

}
