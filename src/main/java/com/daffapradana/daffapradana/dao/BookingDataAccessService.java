package com.daffapradana.daffapradana.dao;

import com.daffapradana.daffapradana.model.Booking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.UUID;

@Repository("postgres")
public class BookingDataAccessService implements BookingDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public BookingDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int createBooking(UUID idRequestBooking, Booking booking) {
        // Query
        final String sql = "INSERT INTO booking (id_request_booking, id_platform, nama_platform, doc_type, term_of_payment) " +
                "VALUES id_request_booking, id_platform, nama_platform, doc_type, term_of_payment";

        // Execute
        jdbcTemplate.update(
                "INSERT INTO booking (id_request_booking, id_platform, nama_platform, doc_type, term_of_payment) " +
                        "VALUES (?, ?, ?, ?, ?)",
                idRequestBooking,
                booking.getIdPlatform(),
                booking.getNamaPlatform(),
                booking.getDocType(),
                booking.getTermOfPayment()
        );

        return 1;
    }

}
