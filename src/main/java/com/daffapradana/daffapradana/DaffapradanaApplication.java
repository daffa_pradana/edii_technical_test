package com.daffapradana.daffapradana;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DaffapradanaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DaffapradanaApplication.class, args);
	}

}
