package com.daffapradana.daffapradana.api;

import com.daffapradana.daffapradana.model.Booking;
import com.daffapradana.daffapradana.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

// Define path
@RequestMapping("api/v1/booking")

// Booking Controller
@RestController
public class BookingController {

    // Refer service
    private final BookingService bookingService;

    // Constructor
    @Autowired
    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    // (POST) Add Booking
    @PostMapping
    public void addBooking(@Valid @NotNull @RequestBody Booking booking) {
        bookingService.addBooking(booking);
    }

}
