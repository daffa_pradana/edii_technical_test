package com.daffapradana.daffapradana.service;

import com.daffapradana.daffapradana.dao.BookingDao;
import com.daffapradana.daffapradana.model.Booking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class BookingService {

    // Attribute
    private final BookingDao bookingDao;

    // Constructor
    @Autowired
    public BookingService(@Qualifier("postgres") BookingDao bookingDao) {
        this.bookingDao = bookingDao;
    }

    // Add Booking
    public int addBooking(Booking booking) {
        return bookingDao.createBooking(booking);
    }
}
